#include <iostream>
#include <opencv2\opencv.hpp>

using namespace cv;
using namespace std;
// tao ma tran anh
Mat video;
VideoCapture cam(0);
String anhmau[5] = { "anhmau0.png","anhmau1.png","12.png","anhmau3.png","anhmau4.png" };
Mat anh_kq;
Mat anh_test;
Mat khung;
Mat anh;
Mat kq;
//ten win
string ten_anh = "My Picture";
string ten_anh_kq = "Results";
String tenmau[10];
//string createtrackbar=" trackbar";
// cac bien dieu khien chuot
Point diem_bd, diem_ht;
bool keo_chuot;
bool di_chuot;
bool chup;
//thuc hien lenh tempalte matching
bool TemplateMatching;
Rect HCN;
int dem = 0;
void MatchingMethod(Mat anh_mau, int R, int G, int B, int n)
{
	int result_cols = video.cols - anh_mau.cols + 1;
	int result_rows = video.rows - anh_mau.rows + 1;
	anh_kq.create(result_cols, result_rows, CV_32FC1);

	matchTemplate(video, anh_mau, anh_kq, 5);
	//threshold(trunggian,trunggian,0.8,1.,CV_THRESH_TOZERO);

	while (1)
	{
		double minVal; double maxVal; Point minLoc; Point maxLoc;
		minMaxLoc(anh_kq, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
		if (maxVal >= 0.8)
		{
			putText(video, tenmau[n], Point(maxLoc.x + anh_mau.cols - 20, maxLoc.y + anh_mau.rows + 20), 2, 1, Scalar(0, 255, 0), 2);
			rectangle(video, maxLoc, Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			rectangle(anh_kq, maxLoc, Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			floodFill(video, maxLoc, Scalar(R, G, B), 0, Scalar(.1), Scalar(1.));
			//imshow( ten_anh, trunggian );
			//imshow( ten_anh_kq, anh_kq );
		}
		else
		{
			break;
			//R=0; B=0; G=0;
		}
	}
	//imshow( ten_anh, video );
	//imshow( ten_anh_kq, anh_kq );
	//return;
}
void dk_chuot(int event, int x, int y, int flags, void* userdata)
{
	if (event == CV_EVENT_LBUTTONDOWN&&keo_chuot == false)
	{
		diem_bd = Point(x, y);
		keo_chuot = true;
	}
	if (event == CV_EVENT_MOUSEMOVE&& keo_chuot == true)
	{
		diem_ht = Point(x, y);
		di_chuot = true;
	}
	if (event == CV_EVENT_LBUTTONUP&& keo_chuot == true)
	{
		HCN = Rect(diem_bd, diem_ht);
		keo_chuot = false;
		di_chuot = false;
		chup = true;
		dem = dem + 1;
	}
}
void chup_anh(Mat frame)
{
	string ten;
	Mat anh_mau;
	if (chup == true)
	{
		cam.read(anh);
		khung = anh(HCN);
		khung.copyTo(anh_mau);
		bool tg = imwrite(anhmau[dem], anh_mau);
		imshow("kq", anh_mau);
		cout << "ten linh kien: ";
		cin >> ten;
		tenmau[dem] = ten;
		chup = false;
	}


	if (di_chuot == true)
	{
		rectangle(frame, diem_bd, Point(diem_ht.x, diem_ht.y), Scalar(0, 255, 0));
	}
}
int main()
{
	Mat test;
	namedWindow(ten_anh);
	namedWindow("test");
	//namedWindow(createtrackbar);
	//goi chuot
	TemplateMatching = false;
	setMouseCallback("test", dk_chuot, &test);
	keo_chuot = false;
	di_chuot = false;
	chup = false;
	while (true)
	{
		Mat anh_mau1 = imread(anhmau[1]);//mau 1
		Mat anh_mau2 = imread(anhmau[2]);// mau 2
		Mat anh_mau3 = imread(anhmau[3]);
		//Mat anh_mau4=imread(anhmau[4]);
		//Mat anh_mau0=imread(anhmau[0]);
		cam.read(test);
		cam.read(video);
		chup_anh(test);

		imshow("test", test);
		if (waitKey(1) == 'q')
		{
			TemplateMatching = !TemplateMatching;
		}
		if (TemplateMatching == true)
		{
			MatchingMethod(anh_mau1, 255, 0, 0, 1);
			MatchingMethod(anh_mau2, 255, 0, 0, 2);
			MatchingMethod(anh_mau3, 255, 0, 0, 3);
			imshow(ten_anh, video);
		}
		waitKey(10);
	}
	//show anh
	//imshow(ten_anh,anh_goc);
	//waitKey(10);
	return 0;
}