#include <opencv2\opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;
Mat destinationImage;

void TemplateMatch(Mat sourceImage, Mat templateImage) {

	double minVal;
	Point minLoc;
	Point tempLoc;

	Mat graySourceImage = Mat(sourceImage.size(), CV_8UC1);
	Mat grayTemplateImage = Mat(templateImage.size(), CV_8UC1);
	Mat binarySourceImage = Mat(sourceImage.size(), CV_8UC1);
	Mat binaryTemplateImage = Mat(templateImage.size(), CV_8UC1);
	Mat destinationImage = Mat(sourceImage.size(), CV_8UC3);

	sourceImage.copyTo(destinationImage);

	cvtColor(sourceImage, graySourceImage, CV_BGR2GRAY);
	cvtColor(templateImage, grayTemplateImage, CV_BGR2GRAY);

	threshold(graySourceImage, binarySourceImage, 200, 255, CV_THRESH_OTSU);
	threshold(grayTemplateImage, binaryTemplateImage, 200, 255, CV_THRESH_OTSU);

	int templateHeight = templateImage.rows;
	int templateWidth = templateImage.cols;

	float templateScale = 0.5f;

	for (int i = 2; i <= 3; i++) {

		int tempTemplateHeight = (int)(templateWidth * (i * templateScale));
		int tempTemplateWidth = (int)(templateHeight * (i * templateScale));

		Mat tempBinaryTemplateImage = Mat(Size(tempTemplateWidth, tempTemplateHeight), CV_8UC1);
		Mat result = Mat(Size(sourceImage.cols - tempTemplateWidth + 1, sourceImage.rows - tempTemplateHeight + 1), CV_32FC1);

		resize(binaryTemplateImage, tempBinaryTemplateImage, Size(tempBinaryTemplateImage.cols, tempBinaryTemplateImage.rows), 0, 0, INTER_LINEAR);

		float degree = 20.0f;

		for (int j = 0; j <= 9; j++) {

			Mat rotateBinaryTemplateImage = Mat(Size(tempBinaryTemplateImage.cols, tempBinaryTemplateImage.rows), CV_8UC1);

			for (int y = 0; y < tempTemplateHeight; y++) {
				for (int x = 0; x < tempTemplateWidth; x++) {
					rotateBinaryTemplateImage.data[y * tempTemplateWidth + x] = 255;
				}
			}


			for (int y = 0; y < tempTemplateHeight; y++) {
				for (int x = 0; x < tempTemplateWidth; x++) {

					float radian = (float)j * degree * CV_PI / 180.0f;
					int scale = y * tempTemplateWidth + x;

					int rotateY = -sin(radian) * ((float)x - (float)tempTemplateWidth / 2.0f) + cos(radian) * ((float)y - (float)tempTemplateHeight / 2.0f) + tempTemplateHeight / 2;
					int rotateX = cos(radian) * ((float)x - (float)tempTemplateWidth / 2.0f) + sin(radian) * ((float)y - (float)tempTemplateHeight / 2.0f) + tempTemplateWidth / 2;

					if (rotateY < tempTemplateHeight && rotateX < tempTemplateWidth && rotateY >= 0 && rotateX >= 0)
						rotateBinaryTemplateImage.data[scale] = tempBinaryTemplateImage.data[rotateY * tempTemplateWidth + rotateX];
				}
			}

			matchTemplate(binarySourceImage, rotateBinaryTemplateImage, result, CV_TM_SQDIFF_NORMED);

			minMaxLoc(result, &minVal, 0, &minLoc, 0, Mat());

			cout << (int)(i * 0.5 * 100) << " , " << j * 20.0 << " , " << (1 - minVal) * 100 << endl;

			if (minVal < 0.15) {
				tempLoc.x = minLoc.x + tempTemplateWidth;
				tempLoc.y = minLoc.y + tempTemplateHeight;
				rectangle(destinationImage, minLoc, tempLoc, CV_RGB(0, 255, 0), 1, 8, 0);
			}
		}
		imshow("ket_qua",result);
	}
	imshow("ket_qua1", destinationImage);
}

int main()
{ 
	//VideoCapture cam(0);
	Mat goc=imread("goc1.jpg");
	//Mat goc;
	Mat mau=imread("mau.jpg");

	/*while (1)
	{
		cam.read(goc);
		TemplateMatch(goc, mau);
		waitKey(10);
	}*/
	TemplateMatch(goc, mau);
	waitKey(0);
	return 0;
}