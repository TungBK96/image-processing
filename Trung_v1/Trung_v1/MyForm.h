#pragma once
#include <opencv2\opencv.hpp>
#include "Header.h"
#include "Header1.h"
#include "Source.cpp"
#include "Source1.cpp"


namespace Trung_v1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	Mat img;
	camera cam;
	str2char stringTochar;
	int s_t, tp, tl;
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  btn_load;
	private: System::Windows::Forms::HScrollBar^  hScrollBar1;
	private: System::Windows::Forms::HScrollBar^  hScrollBar2;
	private: System::Windows::Forms::Button^  btn_process;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::HScrollBar^  hScrollBar3;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->btn_load = (gcnew System::Windows::Forms::Button());
			this->hScrollBar1 = (gcnew System::Windows::Forms::HScrollBar());
			this->hScrollBar2 = (gcnew System::Windows::Forms::HScrollBar());
			this->btn_process = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->hScrollBar3 = (gcnew System::Windows::Forms::HScrollBar());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// btn_load
			// 
			this->btn_load->Location = System::Drawing::Point(282, 61);
			this->btn_load->Name = L"btn_load";
			this->btn_load->Size = System::Drawing::Size(75, 23);
			this->btn_load->TabIndex = 0;
			this->btn_load->Text = L"Load Image";
			this->btn_load->UseVisualStyleBackColor = true;
			this->btn_load->Click += gcnew System::EventHandler(this, &MyForm::btn_load_Click);
			// 
			// hScrollBar1
			// 
			this->hScrollBar1->Location = System::Drawing::Point(36, 22);
			this->hScrollBar1->Name = L"hScrollBar1";
			this->hScrollBar1->Size = System::Drawing::Size(80, 17);
			this->hScrollBar1->TabIndex = 1;
			this->hScrollBar1->Value = 50;
			this->hScrollBar1->ValueChanged += gcnew System::EventHandler(this, &MyForm::sang_toi);
			// 
			// hScrollBar2
			// 
			this->hScrollBar2->Location = System::Drawing::Point(163, 22);
			this->hScrollBar2->Name = L"hScrollBar2";
			this->hScrollBar2->Size = System::Drawing::Size(80, 17);
			this->hScrollBar2->TabIndex = 2;
			this->hScrollBar2->Value = 50;
			this->hScrollBar2->ValueChanged += gcnew System::EventHandler(this, &MyForm::tuong_phan);
			// 
			// btn_process
			// 
			this->btn_process->Location = System::Drawing::Point(282, 90);
			this->btn_process->Name = L"btn_process";
			this->btn_process->Size = System::Drawing::Size(75, 23);
			this->btn_process->TabIndex = 3;
			this->btn_process->Text = L"Process";
			this->btn_process->UseVisualStyleBackColor = true;
			this->btn_process->Click += gcnew System::EventHandler(this, &MyForm::btn_process_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(7, 16);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(255, 235);
			this->pictureBox1->TabIndex = 4;
			this->pictureBox1->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(6, 16);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(255, 235);
			this->pictureBox2->TabIndex = 5;
			this->pictureBox2->TabStop = false;
			// 
			// hScrollBar3
			// 
			this->hScrollBar3->Location = System::Drawing::Point(297, 22);
			this->hScrollBar3->Name = L"hScrollBar3";
			this->hScrollBar3->Size = System::Drawing::Size(80, 17);
			this->hScrollBar3->TabIndex = 6;
			this->hScrollBar3->Value = 50;
			this->hScrollBar3->ValueChanged += gcnew System::EventHandler(this, &MyForm::ty_le);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->pictureBox1);
			this->groupBox1->Location = System::Drawing::Point(14, 55);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(268, 252);
			this->groupBox1->TabIndex = 7;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"IN";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Location = System::Drawing::Point(359, 55);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(268, 251);
			this->groupBox2->TabIndex = 8;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"OUT";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(645, 333);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->hScrollBar3);
			this->Controls->Add(this->btn_process);
			this->Controls->Add(this->hScrollBar2);
			this->Controls->Add(this->hScrollBar1);
			this->Controls->Add(this->btn_load);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void btn_process_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void btn_load_Click(System::Object^  sender, System::EventArgs^  e) {
		OpenFileDialog^ mo_file = gcnew OpenFileDialog();
		mo_file->Filter = "Image(*.bmp; *.jpg)|*.bmp;*.jpg|All files (*.*)|*.*||";
		if (mo_file->ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		{
			return;
		}
		Bitmap^ bmpSrc = gcnew Bitmap(mo_file->FileName);
		pictureBox1->Image = bmpSrc;
		pictureBox1->Refresh();
		img = imread(stringTochar.ConvertString2Char(mo_file->FileName));
		int dbSize_H = img.rows;
		int dbSize_W = img.cols;
	}
private: System::Void sang_toi(System::Object^  sender, System::EventArgs^  e) {
	Mat anh = img;
	anh.convertTo(anh, -1, s_t, tp);
	hScrollBar1->Value = s_t;
	pictureBox1->Image = cam.Show(anh);
}
private: System::Void tuong_phan(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void ty_le(System::Object^  sender, System::EventArgs^  e) {
}
};
}
