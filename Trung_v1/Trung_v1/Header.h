#pragma once

#include <opencv2\opencv.hpp>
#include <iostream>

using namespace cv;
using namespace System::Runtime::InteropServices;

namespace tung
{
	class camera
	{
	public:
		camera();
		System::Drawing::Bitmap^ Show(Mat & colorImage);

	};
	class str2char
	{
	public:
		str2char();
		char * ConvertString2Char(System::String^ str) {	// Marshal method
			char* strs = (char*)(void*)Marshal::StringToHGlobalAnsi(str);
			return strs;
		}
	};
}
