#include <opencv2\opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

void TemplateMatch(Mat sourceImage, Mat templateImage) {

	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point tempLoc;

	Mat graySourceImage = Mat(sourceImage.size(), CV_8UC1);
	Mat grayTemplateImage = Mat(templateImage.size(), CV_8UC1);
	Mat binarySourceImage = Mat(sourceImage.size(), CV_8UC1);
	Mat binaryTemplateImage = Mat(templateImage.size(), CV_8UC1);
	Mat destinationImage = Mat(sourceImage.size(), CV_8UC3);

	sourceImage.copyTo(destinationImage);

	cvtColor(sourceImage, graySourceImage, CV_BGR2GRAY);
	cvtColor(templateImage, grayTemplateImage, CV_BGR2GRAY);

	threshold(graySourceImage, binarySourceImage, 200, 255, CV_THRESH_OTSU);
	threshold(grayTemplateImage, binaryTemplateImage, 200, 255, CV_THRESH_OTSU);

	int templateHeight = templateImage.rows;
	int templateWidth = templateImage.cols;

	float templateScale = 0.5f;

	for (int i = 2; i <= 3; i++) {

		int tempTemplateHeight = (int)(templateWidth * (i * templateScale));
		int tempTemplateWidth = (int)(templateHeight * (i * templateScale));

		Mat tempBinaryTemplateImage = Mat(Size(tempTemplateWidth, tempTemplateHeight), CV_8UC1);
		Mat result = Mat(Size(sourceImage.cols - tempTemplateWidth + 1, sourceImage.rows - tempTemplateHeight + 1), CV_32FC1);

		resize(binaryTemplateImage, tempBinaryTemplateImage, Size(tempBinaryTemplateImage.cols, tempBinaryTemplateImage.rows), 0, 0, INTER_LINEAR);

		float degree = 20.0f;

		for (int j = 0; j <= 2; j++) {

			Mat rotateBinaryTemplateImage = Mat(Size(tempBinaryTemplateImage.cols, tempBinaryTemplateImage.rows), CV_8UC1);
			
			for (int y = 0; y < tempTemplateHeight; y++) {
				for (int x = 0; x < tempTemplateWidth; x++) {
					rotateBinaryTemplateImage.data[y * tempTemplateWidth + x] = 255;
				}
			}

			
			for (int y = 0; y < tempTemplateHeight; y++) {
				for (int x = 0; x < tempTemplateWidth; x++) {

					float radian = (float)j * degree * CV_PI / 180.0f;
					int scale = y * tempTemplateWidth + x;

					int rotateY = -sin(radian) * ((float)x - (float)tempTemplateWidth / 2.0f) + cos(radian) * ((float)y - (float)tempTemplateHeight / 2.0f) + tempTemplateHeight / 2;
					int rotateX = cos(radian) * ((float)x - (float)tempTemplateWidth / 2.0f) + sin(radian) * ((float)y - (float)tempTemplateHeight / 2.0f) + tempTemplateWidth / 2;

					if (rotateY < tempTemplateHeight && rotateX < tempTemplateWidth && rotateY >= 0 && rotateX >= 0)
						rotateBinaryTemplateImage.data[scale] = tempBinaryTemplateImage.data[rotateY * tempTemplateWidth + rotateX];
				}
			}

			matchTemplate(binarySourceImage, rotateBinaryTemplateImage, result, 1);

			//minMaxLoc(result, &minVal, 0, &minLoc, 0, Mat());

			//cout << (int)(i * 0.5 * 100) << " , " << j * 20 << " , " << (1 - minVal) * 100 << endl;

			minMaxLoc(result, 0, &maxVal, 0, &maxLoc, Mat());
			if (maxVal >= 0.98)
			{
				tempLoc.x = maxLoc.x + tempTemplateWidth;
				tempLoc.y = maxLoc.y + tempTemplateHeight;
				//putText(video, tenmau[n], Point(maxLoc.x + anh_mau.cols - 20, maxLoc.y + anh_mau.rows + 20), 2, 1, Scalar(0, 255, 0), 2);
				rectangle(destinationImage, maxLoc, tempLoc, CV_RGB(0, 255, 0), 2, 8, 0);
				rectangle(result, maxLoc, tempLoc, CV_RGB(0, 255, 0), 2, 8, 0);
			}
		}
		
		//imshow("a", result);
	}
	imshow("windowNameDestination", destinationImage);
}
int main()
{

	Mat img = imread("9NaVu.jpg");
	Mat tem = imread("OkxeC.jpg");
	TemplateMatch(img,tem);
	waitKey(0);
	return 0;
}