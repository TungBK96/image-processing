#include <opencv2\opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main()
{
	Mat img = imread("123.jpg");
	Mat origin;
	img.copyTo(origin);
	Mat gray;
	cvtColor(origin, gray, CV_BGR2GRAY);
	GaussianBlur(gray, gray, Size(3, 3), 0);
	Mat edged;
	Canny(gray, edged, 75, 200);


	imshow("1", origin);
	imshow("2", gray);
	imshow("3", edged);
	waitKey(0);

	return 0;
}