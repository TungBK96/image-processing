#include<opencv2/opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

// valid 
Point2f inputQuad[4];
Point2f outputQuad[4];
int i = 0;

void mouse_control(int event, int x, int y, int flags, void* userdata)
{
	if (event == CV_EVENT_RBUTTONDOWN)
	{
		inputQuad[i] = Point(x, y);
		cout << "x=" << x << endl;
		cout << "y=" << y << endl;
		i++;
	}

}

int main()
{
	
	while (1)
	{
		// Lambda Matrix
		Mat lambda(2, 4, CV_32FC1);
		//Input and Output Image;
		Mat input, output;
		output.create(400, 300, CV_32FC1);
		//Load the image
		input = imread("book1.jpg", 1);
		setMouseCallback("Input", mouse_control, NULL);
		// Set the lambda matrix the same type and size as input
		lambda = Mat::zeros(input.rows, input.cols, input.type());


		// The 4 points that select quadilateral on the input , from top-left in clockwise order
		// These four pts are the sides of the rect box used as input 
		/*inputQuad[0] = Point(0, input.rows*0.5 );
		inputQuad[1] = Point(input.cols*0.5, 0);
		inputQuad[2] = Point(input.cols, input.rows*0.5);
		inputQuad[3] = Point(input.cols*0.5, input.rows);*/
		// The 4 points where the mapping is to be done , from top-left in clockwise order
		outputQuad[0] = Point(0, 0);
		outputQuad[1] = Point(300 -1, 0);
		outputQuad[2] = Point(300 -1, 400 -1);
		outputQuad[3] = Point(0, 400-1);

		if (waitKey(0)=='c')
		{
			// Get the Perspective Transform Matrix i.e. lambda 
			lambda = getPerspectiveTransform(inputQuad, outputQuad);
			// Apply the Perspective Transform just found to the src image
			//lambda = getPerspectiveTransform(points, target_points);
			warpPerspective(input, output, lambda, output.size());
			imshow("Output", output);
			i = 0;
		}

		//Display input and output
		imshow("Input", input);
		//waitKey(10);
	}
	return 0;
}