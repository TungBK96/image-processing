#include <iostream>
#include <opencv2\opencv.hpp>
#include <opencv2\xfeatures2d.hpp>

using namespace cv;
using namespace std;
using namespace xfeatures2d;
// tao ma tran anh
Mat video;
VideoCapture cam(1);
String anhmau[5] = { "anhmau0.png","anhmau1.png","12.png","anhmau3.png","anhmau4.png" };
Mat anh_test;
string ten_anh = "My Picture";
String tenmau[10];

// cac bien dieu khien chuot
Point diem_bd, diem_ht;
bool keo_chuot;
bool di_chuot;
bool chup;
//thuc hien lenh tempalte matching
bool TemplateMatching;
Rect HCN;
int dem = 0;
void fearture_matching(Mat img_object, Mat img_scene) {

	cvtColor(img_object, img_object, CV_BGR2GRAY);
	cvtColor(img_scene, img_scene, CV_BGR2GRAY);
	int minHessian = 400;
	Ptr<SURF> detector = SURF::create(minHessian);
	std::vector<KeyPoint> keypoints_object, keypoints_scene;
	Mat descriptors_object, descriptors_scene;
	detector->detectAndCompute(img_object, Mat(), keypoints_object, descriptors_object);
	detector->detectAndCompute(img_scene, Mat(), keypoints_scene, descriptors_scene);
	//-- Step 2: Matching descriptor vectors using FLANN matcher
	FlannBasedMatcher matcher;
	std::vector< DMatch > matches;
	matcher.match(descriptors_object, descriptors_scene, matches);
	double max_dist = 0; double min_dist = 100;
	//-- Quick calculation of max and min distances between keypoints
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		double dist = matches[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}
	printf("-- Max dist : %f \n", max_dist);
	printf("-- Min dist : %f \n", min_dist);
	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	std::vector< DMatch > good_matches;
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		if (matches[i].distance <= 3 * min_dist)
		{
			good_matches.push_back(matches[i]);
		}
	}
	Mat img_matches;
	drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
		good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
	//-- Localize the object
	std::vector<Point2f> obj;
	std::vector<Point2f> scene;
	for (size_t i = 0; i < good_matches.size(); i++)
	{
		//-- Get the keypoints from the good matches
		obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
		scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
	}
	Mat H = findHomography(obj, scene, RANSAC);
	//-- Get the corners from the image_1 ( the object to be "detected" )
	std::vector<Point2f> obj_corners(4);
	obj_corners[0] = cvPoint(0, 0); obj_corners[1] = cvPoint(img_object.cols, 0);
	obj_corners[2] = cvPoint(img_object.cols, img_object.rows); obj_corners[3] = cvPoint(0, img_object.rows);
	std::vector<Point2f> scene_corners(4);
	perspectiveTransform(obj_corners, scene_corners, H);
	//-- Draw lines between the corners (the mapped object in the scene - image_2 )
	line(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[1] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[1] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[2] + Point2f(img_object.cols, 0), scene_corners[3] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[3] + Point2f(img_object.cols, 0), scene_corners[0] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);

	//rectangle(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 2, 8, 0);
	//-- Show detected matches
	imshow("Good Matches & Object detection", img_matches);
}
void MatchingMethod(Mat anh_mau, int R, int G, int B, int n)
{

	Mat anh_kq;
	int result_cols = video.cols - anh_mau.cols + 1;
	int result_rows = video.rows - anh_mau.rows + 1;
	anh_kq.create(result_cols, result_rows, CV_32FC1);
	double minVal; double maxVal; Point minLoc; Point maxLoc;


	matchTemplate(video, anh_mau, anh_kq, 5);


	while (1)
	{
		minMaxLoc(anh_kq, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
		if (maxVal >= 0.8)
		{
			putText(video, tenmau[n], Point(maxLoc.x + anh_mau.cols - 20, maxLoc.y + anh_mau.rows + 20), 2, 1, Scalar(0, 255, 0), 2);
			rectangle(video, maxLoc, Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			rectangle(anh_kq, maxLoc, Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			floodFill(video, maxLoc, Scalar(R, G, B), 0, Scalar(.1), Scalar(1.));
			//imshow( ten_anh, trunggian );
			//imshow( ten_anh_kq, anh_kq );
		}
		else
		{
			break;
		}
	}

}
void dk_chuot(int event, int x, int y, int flags, void* userdata)
{
	if (event == CV_EVENT_LBUTTONDOWN&&keo_chuot == false)
	{
		diem_bd = Point(x, y);
		keo_chuot = true;
	}
	if (event == CV_EVENT_MOUSEMOVE&& keo_chuot == true)
	{
		diem_ht = Point(x, y);
		di_chuot = true;
	}
	if (event == CV_EVENT_LBUTTONUP&& keo_chuot == true)
	{
		HCN = Rect(diem_bd, diem_ht);
		keo_chuot = false;
		di_chuot = false;
		chup = true;
		dem = dem + 1;
	}
}
void chup_anh(Mat frame)
{
	Mat khung;
	Mat anh;
	Mat kq;
	string ten;
	Mat anh_mau;
	if (chup == true)
	{
		cam.read(anh);
		khung = anh(HCN);
		khung.copyTo(anh_mau);
		bool tg = imwrite(anhmau[dem], anh_mau);
		imshow("kq", anh_mau);
		//cout << "ten linh kien: ";
		//cin >> ten;
		//tenmau[dem] = ten;
		chup = false;
	}
	if (di_chuot == true)
	{
		rectangle(frame, diem_bd, Point(diem_ht.x, diem_ht.y), Scalar(0, 255, 0));
	}
}
int main()
{
	Mat test;
	Mat tg;
	namedWindow(ten_anh);
	//namedWindow("test");
	TemplateMatching = false;
	setMouseCallback(ten_anh, dk_chuot, &video);
	keo_chuot = false;
	di_chuot = false;
	chup = false;
	while (true)
	{
		Mat anh_mau1 = imread(anhmau[1]);//mau 1
										 //Mat anh_mau2 = imread(anhmau[2]);// mau 2
										 //Mat anh_mau3 = imread(anhmau[3]);
										 //Mat anh_mau4=imread(anhmau[4]);
										 //Mat anh_mau0=imread(anhmau[0]);
										 //cam.read(test);
		cam.read(video);
		chup_anh(video);

		//imshow("test", test);
		if (waitKey(1) == 'q')
		{
			TemplateMatching = !TemplateMatching;
			fearture_matching(anh_mau1, video);
		}
		//if (TemplateMatching == true)
		//{
			
		//}
		//else
		//{
			imshow(ten_anh, video);
		//}
		waitKey(10);
	}
	return 0;
}