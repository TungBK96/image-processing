#include <iostream>
#include <opencv2\opencv.hpp>

using namespace cv;
using namespace std;
// tao ma tran anh
Mat video;
VideoCapture cam(0);
String anhmau[5] = { "anhmau0.png","anhmau1.png","12.png","anhmau3.png","anhmau4.png" };
Mat anh_test;
string ten_anh = "My Picture";
String tenmau[10];

// cac bien dieu khien chuot
Point diem_bd, diem_ht;
bool keo_chuot;
bool di_chuot;
bool chup;
//thuc hien lenh tempalte matching
bool TemplateMatching;
Rect HCN;
int dem = 0;
void TemplateMatch(Mat sourceImage, Mat templateImage) {

	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point tempLoc;

	Mat graySourceImage = Mat(sourceImage.size(), CV_8UC1);
	Mat grayTemplateImage = Mat(templateImage.size(), CV_8UC1);
	Mat binarySourceImage = Mat(sourceImage.size(), CV_8UC1);
	Mat binaryTemplateImage = Mat(templateImage.size(), CV_8UC1);
	Mat destinationImage = Mat(sourceImage.size(), CV_8UC3);

	sourceImage.copyTo(destinationImage);

	cvtColor(sourceImage, graySourceImage, CV_BGR2GRAY);
	cvtColor(templateImage, grayTemplateImage, CV_BGR2GRAY);

	threshold(graySourceImage, binarySourceImage, 200, 255, CV_THRESH_OTSU);
	threshold(grayTemplateImage, binaryTemplateImage, 200, 255, CV_THRESH_OTSU);

	int templateHeight = templateImage.rows;
	int templateWidth = templateImage.cols;

	float templateScale = 0.5f;

	for (int i = 2; i <= 3; i++) {

		int tempTemplateHeight = (int)(templateWidth * (i * templateScale));
		int tempTemplateWidth = (int)(templateHeight * (i * templateScale));

		Mat tempBinaryTemplateImage = Mat(Size(tempTemplateWidth, tempTemplateHeight), CV_8UC1);
		Mat result = Mat(Size(sourceImage.cols - tempTemplateWidth + 1, sourceImage.rows - tempTemplateHeight + 1), CV_32FC1);

		resize(binaryTemplateImage, tempBinaryTemplateImage, Size(tempBinaryTemplateImage.cols, tempBinaryTemplateImage.rows), 0, 0, INTER_LINEAR);

		float degree = 20.0f;

		for (int j = 0; j <= 9; j++) {

			Mat rotateBinaryTemplateImage = Mat(Size(tempBinaryTemplateImage.cols, tempBinaryTemplateImage.rows), CV_8UC1);

			for (int y = 0; y < tempTemplateHeight; y++) {
				for (int x = 0; x < tempTemplateWidth; x++) {
					rotateBinaryTemplateImage.data[y * tempTemplateWidth + x] = 255;
				}
			}

			
			for (int y = 0; y < tempTemplateHeight; y++) {
			for (int x = 0; x < tempTemplateWidth; x++) {

			float radian = (float)j * degree * CV_PI / 180.0f;
			int scale = y * tempTemplateWidth + x;

			int rotateY = -sin(radian) * ((float)x - (float)tempTemplateWidth / 2.0f) + cos(radian) * ((float)y - (float)tempTemplateHeight / 2.0f) + tempTemplateHeight / 2;
			int rotateX = cos(radian) * ((float)x - (float)tempTemplateWidth / 2.0f) + sin(radian) * ((float)y - (float)tempTemplateHeight / 2.0f) + tempTemplateWidth / 2;

			if (rotateY < tempTemplateHeight && rotateX < tempTemplateWidth && rotateY >= 0 && rotateX >= 0)
			rotateBinaryTemplateImage.data[scale] = tempBinaryTemplateImage.data[rotateY * tempTemplateWidth + rotateX];
			}
			}

			matchTemplate(binarySourceImage, rotateBinaryTemplateImage, result, 1);

			//minMaxLoc(result, &minVal, 0, &minLoc, 0, Mat());

			//cout << (int)(i * 0.5 * 100) << " , " << j * 20 << " , " << (1 - minVal) * 100 << endl;

			minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
			if (minVal <= 0.15)
			{
				tempLoc.x = minLoc.x + tempTemplateWidth;
				tempLoc.y = minLoc.y + tempTemplateHeight;
				//putText(video, tenmau[n], Point(maxLoc.x + anh_mau.cols - 20, maxLoc.y + anh_mau.rows + 20), 2, 1, Scalar(0, 255, 0), 2);
				rectangle(destinationImage, minLoc, tempLoc, CV_RGB(0, 255, 0), 2, 8, 0);
				rectangle(result, minLoc, tempLoc, CV_RGB(0, 255, 0), 2, 8, 0);
			}
		}
	}
	imshow("windowNameDestination", destinationImage);
}
void MatchingMethod(Mat anh_mau, int R, int G, int B, int n)
{
	
	Mat anh_kq;
	int result_cols = video.cols - anh_mau.cols + 1;
	int result_rows = video.rows - anh_mau.rows + 1;
	anh_kq.create(result_cols, result_rows, CV_32FC1);
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	

	matchTemplate(video, anh_mau, anh_kq, 5);
	

	while (1)
	{
		minMaxLoc(anh_kq, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
		if (maxVal >= 0.8)
		{
			putText(video, tenmau[n], Point(maxLoc.x + anh_mau.cols - 20, maxLoc.y + anh_mau.rows + 20), 2, 1, Scalar(0, 255, 0), 2);
			rectangle(video, maxLoc, Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			rectangle(anh_kq, maxLoc, Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			floodFill(video, maxLoc, Scalar(R, G, B), 0, Scalar(.1), Scalar(1.));
			//imshow( ten_anh, trunggian );
			//imshow( ten_anh_kq, anh_kq );
		}
		else
		{
			break;
		}
	}

}
void dk_chuot(int event, int x, int y, int flags, void* userdata)
{
	if (event == CV_EVENT_LBUTTONDOWN&&keo_chuot == false)
	{
		diem_bd = Point(x, y);
		keo_chuot = true;
	}
	if (event == CV_EVENT_MOUSEMOVE&& keo_chuot == true)
	{
		diem_ht = Point(x, y);
		di_chuot = true;
	}
	if (event == CV_EVENT_LBUTTONUP&& keo_chuot == true)
	{
		HCN = Rect(diem_bd, diem_ht);
		keo_chuot = false;
		di_chuot = false;
		chup = true;
		dem = dem + 1;
	}
}
void chup_anh(Mat frame)
{
	Mat khung;
	Mat anh;
	Mat kq;
	string ten;
	Mat anh_mau;
	if (chup == true)
	{
		cam.read(anh);
		khung = anh(HCN);
		khung.copyTo(anh_mau);
		bool tg = imwrite(anhmau[dem], anh_mau);
		imshow("kq", anh_mau);
		//cout << "ten linh kien: ";
		//cin >> ten;
		//tenmau[dem] = ten;
		chup = false;
	}
	if (di_chuot == true)
	{
		rectangle(frame, diem_bd, Point(diem_ht.x, diem_ht.y), Scalar(0, 255, 0));
	}
}
int main()
{
	Mat test;
	Mat tg;
	namedWindow(ten_anh);
	//namedWindow("test");
	TemplateMatching = false;
	setMouseCallback(ten_anh, dk_chuot, &video);
	keo_chuot = false;
	di_chuot = false;
	chup = false;
	while (true)
	{
		Mat anh_mau1 = imread(anhmau[1]);//mau 1
		//Mat anh_mau2 = imread(anhmau[2]);// mau 2
		//Mat anh_mau3 = imread(anhmau[3]);
		//Mat anh_mau4=imread(anhmau[4]);
		//Mat anh_mau0=imread(anhmau[0]);
		//cam.read(test);
		cam.read(video);
		chup_anh(video);

		//imshow("test", test);
		if (waitKey(1) == 'q')
		{
			TemplateMatching = !TemplateMatching;
		}
		if (TemplateMatching == true)
		{
				//MatchingMethod(anh_mau1, 255, 0, 0, 1);
				//MatchingMethod(anh_mau2, 255, 0, 0, 2);
				//MatchingMethod(anh_mau3, 255, 0, 0, 3);
				//imshow(ten_anh, video);
			TemplateMatch(video, anh_mau1);
			
		}
		else
		{
			imshow(ten_anh, video);
		}
		waitKey(10);
	}
	return 0;
}