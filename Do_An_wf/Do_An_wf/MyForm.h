﻿#pragma once

#include <opencv2\opencv.hpp>
#include "Header.h"
#include "Do_an.h"
#include <iostream>
namespace Do_An_wf {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace cv;
	using namespace tung;
	/// <summary>
	/// Summary for MyForm
	VideoCapture cam;
	Mat img;
	camera abc,cba;
	bool Start,quet,xu_ly;
	bool chuot;
	int x = 0, y = 0;
	//cv::Point diem_bd, diem_ht;
	int tun ;
	cv::Point diem_bd, diem_ht;
	bool keo_chuot;
	bool di_chuot;
	bool chup;
	//thuc hien lenh tempalte matching
	bool TemplateMatching;
	Rect HCN;
	string anhmau[5] = { "anhmau0.jpg","anhmau1.jpg","12.jpg","anhmau3.jpg","anhmau4.jpg" };
	//ten win
	string ten_anh = "My Picture";
	string tenmau[10];
	//
	int Device;
	bool D;
	int d ;
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Button^  btn_play;
	private: System::Windows::Forms::Button^  btn_stop;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Button^  btn_exit;
	private: System::Windows::Forms::Button^  btn_quet;
	private: System::Windows::Forms::PictureBox^  pictureBox2;


	private: System::IO::Ports::SerialPort^  serialPort1;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::Button^  btn_xu_ly;


	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Label^  label1;










	private: System::ComponentModel::IContainer^  components;
	protected:


	private:
		/// <summary>
		/// Required designer variable.
		
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->btn_play = (gcnew System::Windows::Forms::Button());
			this->btn_stop = (gcnew System::Windows::Forms::Button());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->btn_exit = (gcnew System::Windows::Forms::Button());
			this->btn_quet = (gcnew System::Windows::Forms::Button());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->serialPort1 = (gcnew System::IO::Ports::SerialPort(this->components));
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->btn_xu_ly = (gcnew System::Windows::Forms::Button());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(7, 19);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(637, 470);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::click);
			this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::move);
			// 
			// btn_play
			// 
			this->btn_play->Location = System::Drawing::Point(671, 12);
			this->btn_play->Name = L"btn_play";
			this->btn_play->Size = System::Drawing::Size(75, 23);
			this->btn_play->TabIndex = 1;
			this->btn_play->Text = L"Play";
			this->btn_play->UseVisualStyleBackColor = true;
			this->btn_play->Click += gcnew System::EventHandler(this, &MyForm::btn_play_Click);
			// 
			// btn_stop
			// 
			this->btn_stop->Location = System::Drawing::Point(671, 41);
			this->btn_stop->Name = L"btn_stop";
			this->btn_stop->Size = System::Drawing::Size(75, 23);
			this->btn_stop->TabIndex = 2;
			this->btn_stop->Text = L"Stop";
			this->btn_stop->UseVisualStyleBackColor = true;
			this->btn_stop->Click += gcnew System::EventHandler(this, &MyForm::btn_stop_Click);
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// btn_exit
			// 
			this->btn_exit->Location = System::Drawing::Point(788, 463);
			this->btn_exit->Name = L"btn_exit";
			this->btn_exit->Size = System::Drawing::Size(87, 45);
			this->btn_exit->TabIndex = 3;
			this->btn_exit->Text = L"Exit";
			this->btn_exit->UseVisualStyleBackColor = true;
			this->btn_exit->Click += gcnew System::EventHandler(this, &MyForm::btn_exit_Click);
			// 
			// btn_quet
			// 
			this->btn_quet->Location = System::Drawing::Point(671, 99);
			this->btn_quet->Name = L"btn_quet";
			this->btn_quet->Size = System::Drawing::Size(75, 23);
			this->btn_quet->TabIndex = 4;
			this->btn_quet->Text = L"quet";
			this->btn_quet->UseVisualStyleBackColor = true;
			this->btn_quet->Click += gcnew System::EventHandler(this, &MyForm::btn_quet_Click);
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(4, 14);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(100, 60);
			this->pictureBox2->TabIndex = 5;
			this->pictureBox2->TabStop = false;
			// 
			// pictureBox3
			// 
			this->pictureBox3->Location = System::Drawing::Point(4, 14);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(100, 60);
			this->pictureBox3->TabIndex = 8;
			this->pictureBox3->TabStop = false;
			// 
			// btn_xu_ly
			// 
			this->btn_xu_ly->Location = System::Drawing::Point(671, 70);
			this->btn_xu_ly->Name = L"btn_xu_ly";
			this->btn_xu_ly->Size = System::Drawing::Size(75, 23);
			this->btn_xu_ly->TabIndex = 9;
			this->btn_xu_ly->Text = L"xu_ly";
			this->btn_xu_ly->UseVisualStyleBackColor = true;
			this->btn_xu_ly->Click += gcnew System::EventHandler(this, &MyForm::btn_xu_ly_Click);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(754, 99);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 12;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(752, 75);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(127, 13);
			this->label3->TabIndex = 13;
			this->label3->Text = L"Số linh kiện có trên mạch";
			// 
			// pictureBox4
			// 
			this->pictureBox4->Location = System::Drawing::Point(4, 14);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(100, 60);
			this->pictureBox4->TabIndex = 14;
			this->pictureBox4->TabStop = false;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->pictureBox1);
			this->groupBox1->Location = System::Drawing::Point(5, 8);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(650, 497);
			this->groupBox1->TabIndex = 18;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Show video";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Location = System::Drawing::Point(667, 204);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(110, 80);
			this->groupBox2->TabIndex = 19;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Linh Kiện 1";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->pictureBox3);
			this->groupBox3->Location = System::Drawing::Point(667, 290);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(110, 80);
			this->groupBox3->TabIndex = 20;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Linh Kiện 2";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->pictureBox4);
			this->groupBox4->Location = System::Drawing::Point(667, 376);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(110, 80);
			this->groupBox4->TabIndex = 21;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Linh kiện 3";
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Web Cam", L"Cam" });
			this->comboBox1->Location = System::Drawing::Point(754, 41);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(121, 21);
			this->comboBox1->TabIndex = 22;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(758, 19);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(71, 13);
			this->label1->TabIndex = 23;
			this->label1->Text = L"Chọn Camera";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(887, 520);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->comboBox1);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->btn_xu_ly);
			this->Controls->Add(this->btn_quet);
			this->Controls->Add(this->btn_exit);
			this->Controls->Add(this->btn_stop);
			this->Controls->Add(this->btn_play);
			this->Name = L"MyForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Do An Phan Loai San Pham";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->groupBox3->ResumeLayout(false);
			this->groupBox4->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
		di_chuot = false;
		keo_chuot = false;
		chup = false;
		TemplateMatching = false;
		quet = false;
		tun = 0;
		Start = false;
		xu_ly = false;
		D = false;
		comboBox1->SelectedIndex = 0;

		

	}
	private: System::Void btn_play_Click(System::Object^  sender, System::EventArgs^  e) {
		if (comboBox1->Text == "Web Cam")
		{
			Device = 0;
		}
		if (comboBox1->Text == "Cam")
		{
			Device = 1;
		}
		cam.open(Device);
		if (cam.isOpened())
		{
			MessageBox::Show("Quet du 3 anh roi an xu ly","Huong dan");
			Start = true;
			timer1->Start();
		}
		else
		{
			MessageBox::Show("Connect Failed");		
		}
		
	}
	private: System::Void btn_stop_Click(System::Object^  sender, System::EventArgs^  e) {
		//cam.open(0);
		Start = false;
		timer1->Stop();
	}
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
		//setMouseCallback(ten_anh, dk_chuot, &video);
		if (Start == true)
		{		
			ct_chinh();
		}
	}
private: Void ct_chinh()
{	
	int lk=0,lk1,lk2,lk3,lk4,lk5;
	Mat anh_mau1 = imread(anhmau[1]);//mau 1
	Mat anh_mau2 = imread(anhmau[2]);// mau 2
	Mat anh_mau3 = imread(anhmau[3]);
	//Mat anh_mau4=imread(anhmau[4]);
	//Mat anh_mau0=imread(anhmau[0]);
	cam.read(video);
	pictureBox1->Size = System::Drawing::Size(video.cols, video.rows);
	chup_anh(video);
	if (TemplateMatching == false)
	{
		pictureBox1->Image = abc.Show(video);
	}
	else
	{
		lk1=MatchingMethod(anh_mau1, 255, 0, 0, 1);
		lk2=MatchingMethod(anh_mau2, 0, 0, 255, 2);
		lk3 = MatchingMethod(anh_mau3, 0, 255, 0, 3);
		//lk4 = MatchingMethod(anh_mau4, 255, 0, 0, 4);
		//lk5 = MatchingMethod(anh_mau0, 255, 0, 0, 5);
		
		lk = lk1 + lk2 + lk3;
		switch (lk)
		{
		case 0:
			textBox3->Text = "0";
			break;
		case 1:
			textBox3->Text = "1";
			break;
		case 2:
			textBox3->Text = "2";
			break;
		case 3:
			textBox3->Text = "3";
			break;
		case 4:
			textBox3->Text = "4";
			break;
		case 5:
			textBox3->Text = "5";
			break;
		}
		pictureBox1->Image = abc.Show(video);
	}
		waitKey(10);				
}
private: Void chup_anh(Mat frame)
{
	Mat khung;
	Mat anh;
	Mat anh_mau;
	string ten1;
	Bitmap^ bmpSrc;
	Bitmap^ bmpSrc1;
	Bitmap^ bmpSrc2;
	if (chup==true)
	{
		cam.read(anh);
		khung = anh(HCN);
		khung.copyTo(anh_mau);
		bool tg = imwrite(anhmau[tun], anh_mau);
		switch (tun)
		{
		case 1:
			bmpSrc = gcnew Bitmap("anhmau1.jpg");
			pictureBox2->Size= System::Drawing::Size(100, 60);
			anh_mau.create(100,60 , CV_32FC1);
			//tenmau[tun] = ten1;
			pictureBox2->Image = bmpSrc;
			//pictureBox2->Refresh();
			chup = false;
			break;
			
		case 2:
			bmpSrc1 = gcnew Bitmap("12.jpg");
			pictureBox2->Size = System::Drawing::Size(100, 60);
			anh_mau.create(100, 60, CV_32FC1);
			pictureBox3->Image = bmpSrc1;
			//pictureBox3->Refresh();
			chup = false;
			break;
		case 3:
			bmpSrc2 = gcnew Bitmap("anhmau3.jpg");
			pictureBox2->Size = System::Drawing::Size(100, 60);
			anh_mau.create(100, 60, CV_32FC1);
			pictureBox4->Image = bmpSrc2;
			//pictureBox4->Refresh();
			chup = false;
			break;
		case 4:
			break;
		}
	}

}
private: int MatchingMethod(Mat anh_mau, int R, int G, int B, int n) {
	Mat anh_kq;
	int result_cols = video.cols - anh_mau.cols + 1;
	int result_rows = video.rows - anh_mau.rows + 1;
	anh_kq.create(result_cols, result_rows, CV_32FC1);

	matchTemplate(video, anh_mau, anh_kq, 5);
	//threshold(trunggian,trunggian,0.8,1.,CV_THRESH_TOZERO);

	while (1)
	{
		double minVal; double maxVal; cv::Point minLoc; cv::Point maxLoc;
		minMaxLoc(anh_kq, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
		if (maxVal >= 0.8)
		{
			putText(video, tenmau[n], cv::Point(maxLoc.x + anh_mau.cols - 20, maxLoc.y + anh_mau.rows + 20), 2, 1, Scalar(0, 255, 0), 2);
			rectangle(video, maxLoc, cv::Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			rectangle(anh_kq, maxLoc, cv::Point(maxLoc.x + anh_mau.cols, maxLoc.y + anh_mau.rows), CV_RGB(R, G, B), 2, 8, 0);
			floodFill(video, maxLoc, Scalar(R, G, B), 0, Scalar(.1), Scalar(1.));
			return 1;
		}
		else
		{
			return 0;
			break;
		}
	}

}
private: System::Void btn_exit_Click(System::Object^  sender, System::EventArgs^  e) {
	if (MessageBox::Show("Are you sure?", "Question", MessageBoxButtons::YesNo, MessageBoxIcon::Information) == System::Windows::Forms::DialogResult::Yes)
	{
		Application::Exit();
	}
}
private: System::Void btn_quet_Click(System::Object^  sender, System::EventArgs^  e) {
	if (quet == false)
	{
		btn_quet->Text = "dung quet";
		quet = true;
	}
	else
	{
		btn_quet->Text = "quet";
		quet = false;
	}
}
private: System::Void click(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if ((quet)&&(keo_chuot==false))
	{
		x = e->X;
		y = e->Y;
		diem_bd = cv::Point(x, y);
		di_chuot = true;
	}
	if((quet)&&(keo_chuot))
	{
		HCN = Rect(diem_bd,diem_ht);
		chup = true;
		tun = tun + 1;
		di_chuot = false;
		keo_chuot = false;
	}
}
private: System::Void move(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if (di_chuot)
	{
		x = e->X;
		y = e->Y;
		diem_ht = cv::Point(x, y);
		rectangle(video, diem_bd, cv::Point(diem_ht.x, diem_ht.y), Scalar(0, 255, 0));
		keo_chuot = true;
	}	
}
private: System::Void btn_xu_ly_Click(System::Object^  sender, System::EventArgs^  e) {
	if (xu_ly == false)
	{
		btn_xu_ly->Text = "dung_xu_ly";
		xu_ly = true;
		TemplateMatching = true;
		quet = false;
		btn_quet->Text = "quet";
		tun = 0;
		di_chuot = false;
		keo_chuot = false;
		chup = false;
	}
	else
	{
		btn_xu_ly->Text = "xu_ly";
		xu_ly = false;
		TemplateMatching = false;
	}	
}
};
}
