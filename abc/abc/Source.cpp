
#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;
class ANH {
private:
	Mat anh = imread("1.jpg");
	Mat dst=anh;
	

public:
	void PhongToThuNhoAnh();
	void TangGiamDoSangDoTuongPhan();
	void ChuyenAnhDenTrang();
	void PhanNguongThreshold();

}anh;
void ANH::PhongToThuNhoAnh() {
	double scale;
	cout << "\nHay nhap ti le thu phong anh:  ";
	cin >> scale;
	Point2f center(anh.cols / 2, anh.rows / 2);
	Mat mat_rot = getRotationMatrix2D(center, 0, scale);
	warpAffine(anh, dst, mat_rot, anh.size());
	imshow("Anh goc", anh);
	imshow("Anh sau phep bien doi", dst);
	//cvWaitKey(0);
}
void ANH::TangGiamDoSangDoTuongPhan() {
	double alpha;
	int beta;
	cout << "\nHay nhap vao do tuong phan alpha: ";
	cin >> alpha;
	cout << "\nHay nhap vao do sang beta: ";
	cin >> beta;
	for (int i = 0; i < anh.rows; i++)
		for (int j = 0; j < anh.cols; j++)
			for (int k = 0; k < 3; k++)
				dst.at<Vec3b>(i, j)[k] = saturate_cast<uchar>(alpha*(anh.at<Vec3b>(i, j)[k]) + beta);
	imshow("anh goc", anh);
	imshow("anh sau khi chinh do tuong phan va do sang", dst);
	//cvWaitKey(0);
}
void ANH::ChuyenAnhDenTrang() {
	Mat grey;
	cvtColor(dst, grey, COLOR_BGR2GRAY);
	imshow("anh goc", anh);
	imshow("anh grayscale", grey);
	//cvWaitKey(0);
	//cvDestroyAllWindows();
}
void ANH::PhanNguongThreshold() {
	double thresh;
	Mat grey;
	cvtColor(dst, grey, COLOR_BGR2GRAY);
	cout << "\nHay nhap vao nguong nhi phan Threshold:  ";
	cin >> thresh;
	fflush(stdin);
	threshold(grey, dst, thresh, 255, CV_THRESH_BINARY);
	imshow("Anh xam goc", grey);
	imshow("Anh nhi phan nguong dong", dst);
	//cvWaitKey(0);
}
void ChonCheDoLamViec() {
	int nhap;
nhaplai:
	cout << "\nBan hay chon che do lam viec mong muon:" << endl;
	cout << "Che do phong to thu nho nhap phim 1 !\nChe do tang giam do sang do tuong phan nhap phim 2 !";
	cout << "\nChe do chuyen anh den trang nhap phim 3 !\nChe do phan nguong threshold nhap phim 4 !" << endl;
	cin >> nhap;
	if (nhap == 1) { anh.PhongToThuNhoAnh(); }
	else if (nhap == 2) { anh.TangGiamDoSangDoTuongPhan(); }
	else if (nhap == 3) { anh.ChuyenAnhDenTrang(); }
	else if (nhap == 4) { anh.PhanNguongThreshold(); }
	else { cout << "\nBan chon sai che do. Nhap lai !  " << endl; goto nhaplai; }
}
int main() {
	while (1)
	{
		if (getchar()=='c')
		{
			ChonCheDoLamViec();
		}
		//getchar();
		cvWaitKey(10);
	}
}
