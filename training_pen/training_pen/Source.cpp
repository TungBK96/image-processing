#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"


#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

/** Function Headers */
void detectAndDisplay(Mat frame);
bool compare_rect(const Rect &a, const Rect &b)
{
	return a.width < b.width;
}

/** Global variables */
String pen = "myfacedetector.xml";
//String eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
//String pen = "myfacedetector.xml";
CascadeClassifier pen_cascade;
CascadeClassifier eyes_cascade;
String window_name = "Capture - Face detection";

/** @function main */
int main(void)
{
	VideoCapture capture;
	Mat frame;
	if (!pen_cascade.load(pen)) { printf("--(!)Error loading face cascade\n"); return -1; };
	//frame = imread("anh1(2).bmp");
	capture.open(1);
	if (!capture.isOpened()) { printf("--(!)Error opening video capture\n"); return -1; }

	while (capture.read(frame))
	{
		if (frame.empty())
		{
			printf(" --(!) No captured frame -- Break!");
			break;
		}

		detectAndDisplay(frame);

		int c = waitKey(10);
		if ((char)c == 27) { break; } 
	}
	return 0;
}


void detectAndDisplay(Mat frame)
{
	std::vector<Rect> faces;
	Mat frame_gray;

	cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	pen_cascade.detectMultiScale(frame_gray, faces, 1.1, 3, 0 | CASCADE_SCALE_IMAGE, Size(25, 25));
	//int i = 15;
	sort(faces.begin(), faces.end(), compare_rect);
	for (size_t i = 0; i < faces.size(); i++)
	{
		Point center(faces[i].x + faces[i].width / 2, faces[i].y + faces[i].height / 2);
		ellipse(frame, center, Size(10,10), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
		rectangle(frame, Rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height), Scalar(255, 0, 0), 3);
	}

	imshow(window_name, frame);
}